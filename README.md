Para usar siga estes passos:

1º Passo:

```
git clone https://Zowder@bitbucket.org/Zowder/megasenaprob.git
```
2º Passo:

Instalar o [Nodejs + Npm](https://nodejs.org/dist/v6.10.3/node-v6.10.3-x64.msi)

3º Passo:

```
Entre no diretório do repositório e digite no terminal:
npm install
```
Após instalar a dependência, executa os comandos: 
```
node filter.js -> Este filter nos dá estatísticas gerais.

node filter2.js -> Ester filter nos retornar estatísticas específicas, como a quantidade de sorteios que tiveram números menores do que 30 sorteados, quantidade de sorteios que tiveram números maiores do que 30 sorteados, entre outras, informações...
```