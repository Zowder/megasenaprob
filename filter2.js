const cheerio = require("cheerio");
const fs = require('fs');

$  = cheerio.load(fs.readFileSync('./resultado_parcial.html'));

$.html()

const data = []
let id = 1

$('tr').each(function(i,tr) {
  const children = $(this).children();
  const concurso = children.eq(0);
  const ganhador = children.eq(9);

  const concursoParse = parseInt(concurso.text().trim())
  const ganhadorParse = parseInt(ganhador.text().trim())

  if(concursoParse && ganhadorParse) {
    const nums = []
    const dataconcurso = children.eq(1);
    const n1 = parseInt(children.eq(2).text().trim())
    const n2 = parseInt(children.eq(3).text().trim())
    const n3 = parseInt(children.eq(4).text().trim())
    const n4 = parseInt(children.eq(5).text().trim())
    const n5 = parseInt(children.eq(6).text().trim())
    const n6 = parseInt(children.eq(7).text().trim())
    nums.push(n1, n2, n3, n4, n5, n6)

    const num30 = nums.filter((num) => {
      return num > 30;
    }) // números maiores que 30;
    const num29 = nums.filter((num) => {
      return num <= 30;
    }) // números menores que 30;
    //console.log(num30)
    const row =
      {
        id: id++,
        concurso: concursoParse,
        data_concurso: dataconcurso.text().trim(),
        ganhadores: ganhadorParse,
        num30: num30,
        num29: num29
      }
    data.push(row)
  }
})
const d_general = []

const d_num = data.filter((obj, i) => {
  const arrs = obj.num29
  if(!arrs.length) {
    return arrs
  }
})

const d_num1 = data.filter((obj, i) => {
  const arrs = obj.num30
  if(!arrs.length) {
    return arrs
  }
})

const filter = data.filter((obj) => {
  const num29 = obj.num29
  const num30 = obj.num30
  if(num29.length > num30.length) {
    return num29
  }
})
const filter2 = data.filter(obj => {
  const num29 = obj.num29
  const num30 = obj.num30
  if(num30.length > num29.length) {
    return num30
  }
})

const filter3 = data.filter(obj => {
  const num29 = obj.num29
  const num30 = obj.num30
  if(num30.length === num29.length) {
    return num29
  }
})
const filter4 = d_num.filter(obj => {
  return obj
})

const filter5 = d_num1.filter(obj => { return obj})
//console.log(filter4.length)
//console.log(filter)
console.log(`de ${data.length} sorteios Apenas ${filter.length} sorteios tiveram números menores do que 30 em sua maioria.`)
console.log(`de ${data.length} sorteios Apenas ${filter2.length} sorteios tiveram números maiores do que 30 em sua maioria.`)
console.log(`de ${data.length} sorteios Apenas ${filter3.length} sorteios tiveram números maiores do que 30 iguais a números menores do que 30.`)
console.log('')
console.log(`de ${data.length} sorteios, apenas ${filter4.length} sorteios tiveram todos os números sorteados maiores do que 30`)
console.log(`de ${data.length} sorteios, apenas ${filter5.length} sorteios tiveram todos os números sorteados menores do que 30`)
//console.log(JSON.stringify(d_num1))
//console.log(JSON.stringify(arr1))
//console.log(data.length)
